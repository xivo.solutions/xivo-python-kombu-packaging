# xivo-python-kombu-packaging

Debian packaging for [kombu](https://kombu.readthedocs.org) used in XiVO.

## Upgrading

To upgrade kombu:

* Update the version number in the `VERSION` file
* Update the changelog using `dch -i` to the matching version
* Push the changes

